//
//  ViewController.swift
//  HookahInfo
//
//  Created by Rostik on 25.12.18.
//  Copyright © 2018 Rostyslav. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var button: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
    }

    @IBAction func buttonAction(_ sender: Any) {
        print("Hello")
    }
    
}

